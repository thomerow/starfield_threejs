function addEvent(obj, evType, fn) { 
    var r;
    
    if (obj.addEventListener) { 
        obj.addEventListener(evType, fn, false); 
        return true; 
    } 
    else if (obj.attachEvent) { 
        r = obj.attachEvent('on' + evType, fn); 
        return r; 
    } 
    else return false; 
}

var container, stats;
var camera, scene, renderer, particles, geometry;

var StarSize = 4;
var StarCount = 5000;
var Distance = 1000;
var VertRange = { Min: -(Distance * 1.5), Max: (Distance * 1.5)};
var HorRange = { Min: -(Distance * 2), Max: (Distance * 2)};
var TargetSpeed = 1200;     // Distance per second
var Speed = 0;
var Accel = 50;

addEvent(window, 'load', function() {
    if (!Detector.webgl) {
        Detector.addGetWebGLMessage();
        return;
    }
    init();
    animate();
});

function Rand(from, to) {
    return (Math.random() * (to - from)) + from;
}

function getRandomStarPos() {
    var pos = { 
        X: Rand(HorRange.Min, HorRange.Max),
        Y: Rand(VertRange.Min, VertRange.Max), 
        Z: Rand(Distance, -Distance),
        // Z: Rand(-Distance, -Distance),    // Debug
    };
        
    return pos;
}

function init() {
    container = document.createElement('div');
    document.body.appendChild(container);

    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 3000);
    camera.position.z = Distance;
    
    scene = new THREE.Scene();
    // scene.fog = new THREE.Fog(0x000000, (Distance * 2) - 300, (Distance * 2) + 100);
    scene.fog = new THREE.FogExp2(0x000000, 0.0007);
    geometry = new THREE.Geometry();

    for (i = 0; i < StarCount; i ++) {
        var pos = getRandomStarPos();
        var vertex = new THREE.Vector3();
        vertex.x = pos.X;
        vertex.y = pos.Y;
        vertex.z = pos.Z;

        geometry.vertices.push( vertex );
    }

    var material = new THREE.ParticleSystemMaterial({ size: StarSize });
    particles = new THREE.ParticleSystem(geometry, material); 
    scene.add(particles);

    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    // addStats();

    addEvent(window, 'resize', onWindowResize);
}

function addStats() {
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    container.appendChild(stats.domElement);
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
    requestAnimationFrame(animate);

    render();
    if (stats) stats.update();
}

var lastTime = Date.now();

function render() {
    var delta_t = Date.now() - lastTime;
    if (delta_t == 0) return;    
    lastTime = Date.now();

    delta_t /= 1000;    // Convert time difference to seconds
    var delta_path = delta_t * Speed;
    
    if (Speed < TargetSpeed) Speed += Accel * delta_t;

    for (var i = 0; i < StarCount; ++i) {
        var star = particles.geometry.vertices[i];
        
        star.z += delta_path;
        if (star.z > Distance) {
            var pos = getRandomStarPos();
            
            star.x = pos.X;
            star.y = pos.Y;
            
            // The following ensures even star distribution on z axis after long interruptions
            // (e. g. tab switches)
            star.z = -Distance + (star.z - Distance);
        }
    }

    particles.geometry.verticesNeedUpdate = true;    
    renderer.render(scene, camera);
}
